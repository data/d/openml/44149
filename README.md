# OpenML dataset: Heart_disease_prediction_20

https://www.openml.org/d/44149

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This database contains 14 attributes.     The goal field refers to the presence of heart disease in the patient.    It is integer valued with 0 or 1.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44149) of an [OpenML dataset](https://www.openml.org/d/44149). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44149/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44149/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44149/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

